_ = require('underscore')

has = (prop, obj) ->
  return obj.hasOwnProperty prop

isCurve = _.partial(has, 'angle')

isSwitch = _.partial(has, 'switch')

getRadiusWithLane = (curve, lane) ->
  return (Math.abs(curve.angle) * Math.PI * (curve.radius + lane.distanceFromCenter))/180

getCurveLength = (piece, lane) ->
  return (piece.angle * Math.PI * piece.radius)/180

getPieceLength = (piece, lane) ->
  if isCurve(piece)
    if lane
      getRadiusWithLane(piece, lane)
    else
      getCurveLength(piece)
  else
    piece.length

getLaneChangeDirection = (currentLaneIndex, destinationLaneIndex) ->
  if currentLaneIndex < destinationLaneIndex
    'Right'
  else
    'Left'

degToRad = (deg) ->
  return (deg * Math.PI)/180

ensureInRange = (index, range) ->
  return index % range

module.exports =
  isCurve: isCurve
  isSwitch: isSwitch
  getCurveLength: getCurveLength
  getLaneChangeDirection: getLaneChangeDirection
  degToRad: degToRad
  ensureInRange: ensureInRange
  getPieceLength: getPieceLength
  getRadiusWithLane: getRadiusWithLane

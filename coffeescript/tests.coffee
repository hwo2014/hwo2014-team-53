trackFixture = require('./track-fixture')

Track = require('./track')
Car = require('./car')

isInnerLaneTests = ->
  track = new Track(trackFixture)

  rightCurve = angle: 45
  leftCurve = angle: -45
  rightLane = distanceFromCenter: 30
  leftLane = distanceFromCenter: -30

  throw "isInnerLane(rightCurve, rightLane) == true" unless track._isInnerLane(rightCurve, rightLane) == true
  throw "isInnerLane(rightCurve, leftLane) == false" unless track._isInnerLane(rightCurve, leftLane) == false
  throw "isInnerLane(leftCurve, rightLane) == false" unless track._isInnerLane(leftCurve, rightLane) == false
  throw "isInnerLane(leftCurve, leftLane) == true" unless track._isInnerLane(leftCurve, leftLane) == true

getPieceLengthTests = ->
  track = new Track(trackFixture)

  throw "getPieceLength(11, 1) == 82.46680715673207" unless track.getPieceLength(11, 1) == 82.46680715673207

setNewVelocityTests= ->
  track = new Track(trackFixture)
  car = new Car(track)

  positions = [
    {
      index: 39
      distance: 1
    },
    {
      index: 0
      distance: 2
    }
  ]
  car._setNewVelocity(positions)
  throw "car._setNewVelocity" unless car.getVelocity() == 91

isInnerLaneTests()
getPieceLengthTests()
setNewVelocityTests()

console.log "Success"

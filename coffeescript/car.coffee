Bacon      = require('bacon.model')
_          = require('underscore')

utils = require('./utils')
acceleration = require('./acceleration.coffee')

class Car
  constructor: (@track) ->
    @angleStream = new Bacon.Bus()
    @positionStream = new Bacon.Bus()

    @angle = Bacon.Model([0, 0])
    @angle.addSource(@angleStream.slidingWindow(2, 2))

    @velocity = Bacon.Model(0)
    @oldVelocity = Bacon.Model(0)

    @throttle = Bacon.Model(1)
    @pieceIndex = Bacon.Model(0)
    @currentLaneIndex = Bacon.Model()
    @endLaneIndex = Bacon.Model()
    @oldAngleDerivate = 0
    @a_max = 0
    @inPiecePosition = 0

    @currentState = Bacon.Model.combine {
      velocity: @velocity,
      oldVelocity: @oldVelocity,
      throttle: @throttle,
      pieceIndex: @pieceIndex
      currentLaneIndex: @currentLaneIndex
      endLaneIndex: @endLaneIndex
    }

    @carDataStream = new Bacon.Bus()
    @carDataStream.onValue (data) =>
      @pieceIndex.set(data.piecePosition.pieceIndex)
      @currentLaneIndex.set(data.piecePosition.lane.startLaneIndex)
      @endLaneIndex.set(data.piecePosition.lane.endLaneIndex)
      @angleStream.push(data.angle)

      @positionStream.push index: data.piecePosition.pieceIndex, distance: data.piecePosition.inPieceDistance

      @_setNewAccelerations()

    # Positions contains two items, last and current position
    # One position has form { index: pieceIndex, distance: inPieceDistance }
    @positionStream.slidingWindow(2, 2).onValue (positions) =>
      @_setNewVelocity(positions)
      @_setInPieceDistance(positions)

  _setInPieceDistance: (positions) ->
    @inPiecePosition = positions[0].distance

  getInPieceDistance: ->
    return @inPiecePosition

  updateCarPosition: (data) ->
    @carDataStream.push data

  getCurrentPieceIndex: ->
    @pieceIndex.get()

  getThrottle: ->
    @throttle.get()

  setThrottle: (throttle)->
    @throttle.set(throttle)

  getVelocity: ->
    @velocity.get()

  getCurrentLaneIndex: ->
    @currentLaneIndex.get()

  getEndLaneIndex: ->
    @endLaneIndex.get()

  getAngle: ->
    _.last(@angle.get())

  getAngleDerivate: ->
    a = @angle.get()
    if @getAngle() > 0
      a[1] - a[0]
    else
      -(a[1] - a[0])

  getAngleAcceleration: ->
    angleAcceleration = @getAngleDerivate() - @oldAngleDerivate
    console.log "old anglederivate", angleAcceleration
    @oldAngleSpeed = angleAcceleration
    return angleAcceleration

  setMaxAcceleration: () ->
    @a_max =  Math.max(@a_max, (@velocity.get() - @oldVelocity.get()))
    return @a_max

  velocityDerivate: (v1, v2) ->
    return v2 - v1

  _setNewVelocity: (positions) ->
    getDistanceBetween = (firstPos, secondPos) =>
      if firstPos.index == secondPos.index
        secondPos.distance - firstPos.distance
      else
        remainingDistance = @track.getPieceLength(firstPos.index, @getCurrentLaneIndex()) - firstPos.distance
        nextIndex = if @track.getPieceCount() <= firstPos.index + 1
          0
        else
          firstPos.index + 1

        remainingDistance + getDistanceBetween({index: nextIndex, distance: 0}, secondPos)

    @oldVelocity.set(@velocity.get())
    @velocity.set(getDistanceBetween(positions[0], positions[1]))


  _setNewAccelerations: ->

module.exports = Car

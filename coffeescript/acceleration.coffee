_ = require('underscore')

maxacceleration = 0
acceleration = 0

accelerations = []

slidingWindow = (arr, fn, n) ->
  return arr.concat(fn()).slice(-n)

getAcceleration = -> return acceleration
updateMaxAcceleration = () ->
  maxacceleration = _.reduce(accelerations, ((memo, num) -> return Math.max(memo, num)), 0)
  acceleration = maxacceleration if maxacceleration > acceleration

setNewAccelerations = (v_2, v_1) ->
  accelerations = slidingWindow(accelerations, (-> return v_2 - v_1))

module.exports = {
  updateMaxAcceleration: updateMaxAcceleration,
  setNewAccelerations: setNewAccelerations
  getAcceleration: getAcceleration
}

utils = require('./utils')
_ = require('underscore')

class Turbo
  constructor: (@car, @track) ->
    @params = {}

  shouldUseTurbo: ->
    straight = @track.getStraightLength(@car.getCurrentPieceIndex())
    if straight > 400 and @car.getThrottle() is 1
      return not _.isEmpty(@params)
    else
      return false

  setTurboUsed: ->
    @params = {}

  setTurboParams: (params) ->
    @params = params


module.exports = Turbo

_ = require('underscore')
utils = require('./utils')

isSameAsPrevious = (piece, previous) ->
  if utils.isCurve(piece)
    return piece.angle is previous?.angle and piece.radius is previous?.radius

# Immutable class for storing and retrieving track information
class Track
  constructor: (@track) ->
    @lanes = @track.lanes
    @pieces = @track.pieces
    @curves = @_findoutCurves(_.clone(@pieces))
    @consecutiveCurves = @_findoutConsecutiveCurves(_.clone(@curves))
    @switches = @_findoutSwitches(_.clone(@pieces))

  getPiece: (index) ->
    @pieces[index]

  getNextPiece: (index) ->
    return @getPiece(utils.ensureInRange(index + 1, @pieces.length-1))

  getPieceCount: ->
    @pieces.length

  getNextCurve: (index) ->
    pieces = @pieces.slice(index).concat(@pieces)
    ret = _.find(pieces, (piece) ->
      return utils.isCurve(piece))

  getStraightLength: (index) ->
    nextCurve = @getNextCurve(index)
    if nextCurve.index < index
      endIndex = @pieces.length + nextCurve.index
      pieces = @pieces.concat(@pieces).slice(index, endIndex)
    else
      pieces = @pieces.slice(index, nextCurve.index)
    return _.reduce(pieces, (memo, piece) ->
      return memo + piece.length
    , 0)
  getNextSwitch: (index) ->
    pieces = @switches.slice(index).concat(@switches)
    ret = _.find(pieces, (piece) ->
      return utils.isSwitch(piece))

  getNextConsecutiveCurve: (pieceIndex) ->
    absDistance = (a, b) ->
      return Math.abs(b-a)

    _.reduce(@consecutiveCurves, (memo, curve) ->
      if absDistance(memo?.index, pieceIndex) < absDistance(curve?.index, pieceIndex)
        return memo
      else
        return curve
    , {})

  getLeftMostLane: ->
    _.min(@lanes, (l) -> l.distanceFromCenter)

  getRightMostLane: ->
    _.max(@lanes, (l) -> l.distanceFromCenter)

  getPieceLength: (pieceIndex, laneIndex) ->
    piece = @getPiece(pieceIndex)
    lane = _.find(@lanes, (l) -> l.index == laneIndex)
    if utils.isCurve(piece)
      if @_isInnerLane(piece, lane)
        (Math.abs(piece.angle) * Math.PI * (piece.radius - Math.abs(lane.distanceFromCenter))) / 180
      else
        (Math.abs(piece.angle) * Math.PI * (piece.radius + Math.abs(lane.distanceFromCenter))) / 180
    else
      piece.length

  getConsecutivePieceLength: (pieceIndex, laneIndex) ->
    piece = @getPiece(pieceIndex)
    lane = _.find(@lanes, (l) -> l.index == laneIndex)
    if utils.isCurve(piece)
      if @_isInnerLane(piece, lane)
        (Math.abs(piece.angle * piece.count) * Math.PI * (piece.radius - Math.abs(lane.distanceFromCenter))) / 180
      else
        (Math.abs(piece.angle * piece.count) * Math.PI * (piece.radius + Math.abs(lane.distanceFromCenter))) / 180
    else
      piece.length

  _isInnerLane: (piece, lane) ->
    isPositiveLane = lane.distanceFromCenter > 0 # positive -> right
    isPositiveAngle = piece.angle > 0 # positive -> right

    isPositiveLane == isPositiveAngle

  _findoutCurves: (pieces) ->
    curves = []
    countConsecutivePiecesFrom = (piece, i) =>
      next = @getPiece(i + 1)
      if isSameAsPrevious(next, piece)
        return countConsecutivePiecesFrom(next, (i+1)) + 1
      else
        return 1

    _.each(pieces, (piece, index, list) ->
      if utils.isCurve(piece)
        piece.count = countConsecutivePiecesFrom(piece, index)
        piece.index = index
        curves.push piece
    )
    return curves

  _findoutConsecutiveCurves: (curves) ->
    consecutiveCurves = _.chain(curves)
      .map((curve) ->
        return _.extend(curve, {
          remainingLength: Math.abs((curve.angle * curve.count * Math.PI * curve.radius)/180)
        })
      ).value()
    return consecutiveCurves

  _findoutSwitches: (pieces) ->
    return _.map(pieces, (piece, index) ->
      if utils.isSwitch(piece)
        piece.index = index
      return piece
    )


module.exports = Track

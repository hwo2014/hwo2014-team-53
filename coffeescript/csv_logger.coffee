_ = require 'underscore'
fs = require 'fs'
moment = require 'moment'

class CsvLogger
  constructor: ->
    @values = {}
    @headers = []
    @fileName = "logs/#{moment().format("D_MM_H_m_s")}.csv"

  writeHeaders: (headers...) ->
    @headers = headers
    fs.appendFileSync @fileName, "#{@headers.join(',')}\n"

  log: (header, value = 0) ->
    if _.contains(@headers, header)
      @values[header] = value
    else
      throw "Header #{header} is unknown. Headers should be set with writeHeaders-function"

  flush: (toStdOut) ->
    logLine = "#{(@values[header] for header in @headers).join(',')}\n"
    fs.appendFile @fileName, logLine, (err) ->
      if err then throw err
    if toStdOut
      for header in @headers
        console.log "#{header}: #{@values[header]}"

    for key in _.keys(@values)
      @values[key] = undefined

csvLogger = new CsvLogger()

module.exports = csvLogger

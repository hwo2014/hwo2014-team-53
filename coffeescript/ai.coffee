Throttle = require('./throttle')
Turbo = require('./turbo')
Switch = require('./switch')
acceleration = require('./acceleration.coffee')
utils = require('./utils')

MAX_VELOCITY = 10

class AI
  constructor: (@car, @track) ->
    @maxAngle = 60
    @throttle = new Throttle(10, @car)
    @switch = new Switch(@car, @track)
    @turbo = new Turbo(@car, @track)

  # Note: This will alter ourCar state according to what the AI decides
  decide: (sendFunc, tick) ->
    currentPiece = @track.getPiece(@car.getCurrentPieceIndex())
    nextPiece = @track.getNextPiece(@car.getCurrentPieceIndex())
    if @switch.shouldSwitch(nextPiece, currentPiece, tick)
      console.log "SWIIIIIIIIIIIITTTTTCCCHHHHH"
      return sendFunc {msgType: "switchLane", data: @switch.getDirection()}
    if @turbo.shouldUseTurbo()
      console.log "TURBOOOOOOOOOOOOOOOOOOOOOOO!"
      @turbo.setTurboUsed()
      return sendFunc {msgType: "turbo", data: "CHOOOOOO CHOOOOO!"}
    else
      nextThrottle = @_getThrottleForPiece(nextPiece)
      @car.setThrottle(nextThrottle)
      sendFunc {msgType: "throttle", data: nextThrottle}

  turboAvailable: (data)->
    @turbo.setTurboParams(data)

  _getEstimatedAngle: () ->
    w = @car.getAngleDerivate() # anglespeed at start
    s = @track.getPieceLength(@car.getCurrentPieceIndex(), @car.getCurrentLaneIndex()) - @car.getInPieceDistance() # remaining distance in piece
    v = @car.getVelocity() # car velocity
    a = @car.getAngleAcceleration() # angle acceleration

    t = s/v
    console.log "anglespeed #{w}, distance #{s}, velocity #{v}, angle acceleration #{a}, time #{t}"
    # with current velocity following angle is achieved at end
    # of curve
    angle = w*t + 0.5*a*Math.pow(t, 2)
    return angle

  _getThrottleForPiece: (piece) ->
    if utils.isCurve(piece)
      @throttle.setTargetVelocity(@_getMaxVelocityForCurve(piece))
    else
      @throttle.setTargetVelocity(MAX_VELOCITY)

    @throttle.updateCurrentVelocity(@car.getVelocity())
    @throttle.getThrottle()

  _getMaxVelocityForCurve: (curve) ->
    angle = Math.abs(curve.angle)
    radius = curve.radius
    carAngle = Math.abs(@car.getAngle())
    carAngleDerivate = @car.getAngleDerivate()
    carVelocity = @car.getVelocity()
    estimateAngle = Math.abs(@_getEstimatedAngle())
    console.log "angle estimate", estimateAngle

    currentRadius = @track.getPieceLength(curve.index, @car.getCurrentLaneIndex())
    maxSpeed = Math.sqrt(Math.abs(@car.a_max*(currentRadius/utils.degToRad(curve.angle)))) - 0.5
    console.log "max speed", maxSpeed
    curveMinVelocity = maxSpeed
    desired = 0
    if (estimateAngle > 58) || carAngleDerivate > 2.3 || carVelocity - curveMinVelocity > 3
      desired = curveMinVelocity - 0.5
    else
      desired = MAX_VELOCITY

    if carAngleDerivate >= 3
      desired = desired - (carAngleDerivate / 3)
    if carAngleDerivate > 5
      desired = desired / 2
    console.log "desired", desired
    desired

module.exports = AI

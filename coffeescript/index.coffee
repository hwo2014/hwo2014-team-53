net        = require("net")
JSONStream = require('JSONStream')
_          = require('underscore')
Bacon      = require('bacon.model')

logger = require('./csv_logger')

acceleration = require('./acceleration.coffee')

Car = require('./car')
Track = require('./track')
AI = require('./ai')

utils = require('./utils')

serverHost = process.argv[2]
serverPort = process.argv[3]
botName = process.argv[4]
botKey = process.argv[5]

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort)

logger.writeHeaders('tick', 'throttle', 'velocity', 'angle', 'angle_d', "target_velocity", "control_variable")

client = net.connect serverPort, serverHost, () ->
  send({ msgType: "join", data: { name: botName, key: botKey }})

send = (json) ->
  client.write JSON.stringify(json)
  client.write '\n'

jsonStream = client.pipe(JSONStream.parse())

getOurCar = (data) ->
  sameColor = (c) -> return c.id.color is car.color
  return _.find(data.data, sameColor)

car = null
ourCar = null
track = null
ai = null

# Returns undefined if no switch is needed
getLaneSwithDirection = (currentPieceIndex, currentLaneIndex) ->
  if switchHandler
    bestLane = switchHandler.bestLane(currentPieceIndex)
    if bestLane.index != currentLaneIndex
      switchHandler.getChangeDirection(currentLaneIndex, bestLane.index)

jsonStream.on 'data', (data) ->
  if data.msgType == 'spawn'
    console.log "Spawned"
  if data.msgType == 'crash'
    console.log "CRAAAASSSHHH"
    ai.maxAngle = ourCar.getAngle()
  if data.msgType == 'carPositions'
    console.log "-- tick --"
    ourCar.updateCarPosition(getOurCar(data))
    console.log "Piece index: #{ourCar.getCurrentPieceIndex()}"

    # starting acceleration
    if data.gameTick < 50 then ourCar.setMaxAcceleration()

    logger.log 'tick', data.gameTick
    logger.log 'throttle', ourCar.getThrottle()
    logger.log 'velocity', ourCar.getVelocity()
    logger.log 'angle', ourCar.getAngle()
    logger.log 'angle_d', ourCar.getAngleDerivate()
    logger.flush(true)

    ai.decide(send, data.gameTick)
  if data.msgType is 'turboAvailable'
    ai.turboAvailable(data.data)
    ai.decide(send, data.gameTick)
  else
    if data.msgType == 'join'
      console.log 'Joined'
    else if data.msgType == 'gameStart'
      console.log 'Race started'
    else if data.msgType == 'gameEnd'
      console.log 'Race ended'
    else if data.msgType == 'gameInit'
      console.log 'game initialized'
      track = new Track(data.data.race.track)
      ourCar = new Car(track)
      ai = new AI(ourCar, track)
    else if data.msgType == 'yourCar'
      car = data.data
    else if data.msgType == 'lapFinished'
      console.log "lap #{data.data.lapTime.lap} finished"
      console.log "lap time: #{data.data.lapTime.millis}"

    send {msgType: "ping", data: {}}

jsonStream.on 'error', ->
  console.log "disconnected"

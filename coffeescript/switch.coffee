utils = require('./utils')
_ = require('underscore')

isSwitching = false
bestLaneIndex = null
class Switch
  constructor: (@car, @track) ->
    @lanes = @track.lanes
    @lanesCount = @track.lanes.length
    @pieces = @track.pieces

  _calculateLaneLength: (lane) ->
    index = @car.getCurrentPieceIndex()
    nextSwitch = @track.getNextSwitch(index)
    if nextSwitch.index < index
      pieces = @pieces.slice(index, @track.length)
    else
      pieces = @pieces.slice(index, nextSwitch.index)
    return _.reduce(pieces, (memo, piece) ->
      return memo + utils.getPieceLength(piece, lane)
    , 0)

  _getOptimalLane: (lanes) ->
    lengths = _.map(lanes, (lane) =>
      return @_calculateLaneLength(lane)
    ).reverse()
    # failsafe if all the lanes has the
    # same length -> getInnermostLane
    if _.uniq(lengths).length is 1
      return @_getInnermostLane().index
    return lengths.indexOf(_.min(lengths))

  _getInnermostLane: ->
    nextCurve = @track.getNextCurve(@car.getCurrentPieceIndex())
    if nextCurve.angle < 0 # Negative -> left turn -> leftMostLane
      @track.getLeftMostLane()
    else
      @track.getRightMostLane()

  _getOutermostLane: ->
    nextCurve = @track.getNextCurve(@car.getCurrentPieceIndex())
    if nextCurve.angle < 0 # Negative -> left turn -> rightMostLane
      @track.getRightMostLane()
    else
      @track.getLeftMostLane()

  getDirection: ->
    direction = utils.getLaneChangeDirection(@car.getCurrentLaneIndex(), bestLaneIndex)
    return direction

  shouldSwitch: (nextPiece, currentPiece, tick) ->
    endLaneIndex = @car.getEndLaneIndex()
    currentLaneIndex = @car.getCurrentLaneIndex()
    if endLaneIndex isnt currentLaneIndex
      isSwitching = false
    if tick > 0 and nextPiece.switch and not isSwitching
      if utils.isCurve(nextPiece) and utils.isCurve(currentPiece)
        bestLaneIndex = @_getOutermostLane().index
      else
        bestLaneIndex = @_getOptimalLane(@lanes)

      isSwitching = bestLaneIndex isnt currentLaneIndex
      return isSwitching
    return false

module.exports = Switch

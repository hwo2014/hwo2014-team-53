PIDController = require('node-pid-controller')
logger = require('./csv_logger')

class Throttle
  constructor: (initialTarget, car) ->
    @pidController = new PIDController(3.5, 0, 0) # p, i, d
    @pidController.setTarget(initialTarget)

    @currentThrottle = 0
    @targetVelocity = 0
    @car = car

  setTargetVelocity: (velocity) ->
    logger.log "target_velocity", velocity
    @targetVelocity = 0
    @pidController.setTarget(velocity)

  updateCurrentVelocity: (velocity) ->
    controlVariable = @pidController.update(velocity)
    logger.log "control_variable", controlVariable
    @currentThrottle = @_getThrottleFromControlVariable(controlVariable)

  getThrottle: ->
    @currentThrottle

  # Returns actual throttle value from control variable
  # I think this could be made a lot of better, now it will try to achieve the target value
  # as fast as possible, and only the last 50% is achieved gracefully.
  _getThrottleFromControlVariable: (controlVariable) ->
    throttle = @currentThrottle + (controlVariable / 100)
    if throttle < 0 then throttle = 0
    if throttle > 1 then throttle = 1
    throttle

module.exports = Throttle
